import hashlib
from unittest import TestCase
from unittest.mock import MagicMock, patch

from utils.rss_reader import read_feed


def mock_md5():
    return MagicMock(**{'return_value': hashlib.md5("equal_hash".encode('utf-8'))})


class ReadFeedTestCase(TestCase):

    def setUp(self):
        self.path = "https://lifehacker.com/rss"

    def test_with_unequal_hash(self):
        unequal_hash = 'unequal_hash'

        result = read_feed(self.path, unequal_hash)

        self.assertIsNotNone(result)
        self.assertIn('hash', result)

    def test_with_equal_hash(self):
        with patch('hashlib.md5', mock_md5()):
            equal_hash = hashlib.md5('equal_hash'.encode('utf-8')).hexdigest()

            result = read_feed(self.path, equal_hash)

            self.assertIsNone(result)
