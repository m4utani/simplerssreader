import hashlib
import json
from logging import getLogger

import feedparser

logger = getLogger(__name__)


def read_feed(path: str, last_hash):
    """
    return feed from path.
    """

    d = feedparser.parse(path)

    if d.status == 200:
        feed_hash = hashlib.md5(json.dumps(d).encode("utf-8")).hexdigest()

        if feed_hash == last_hash:
            logger.info('Feed has not changed since the last reading.')
            return None

        d.update({'hash': feed_hash})
        return d

    if d.bozo:
        logger.error("Feed {path}: {bozo_exception}".format(path=path,bozo_exception=d.bozo_exception))
        return None

    logger.error("Feed {path}: got HTTP {status}".format(path=path, status=d.status))
    return None
