from django.db import models


class BaseModel(models.Model):
    created = models.DateTimeField(verbose_name="Created At", null=True, auto_now_add=True)
    updated = models.DateTimeField(null=True, auto_now=True)

    class Meta:
        abstract = True
