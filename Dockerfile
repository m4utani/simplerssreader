FROM python:3.8.0
ENV PYTHONUNBUFFERED 1

RUN mkdir -p /home/rss_reader

ENV HOME=/home/rss_reader
ENV APP_HOME=/home/rss_reader/web

RUN mkdir $APP_HOME
RUN mkdir $APP_HOME/staticfiles
RUN mkdir $APP_HOME/assets
WORKDIR $APP_HOME

COPY . $APP_HOME

RUN pip install -r ./requirements.txt