from django.contrib import admin

from rss_reader_app.models import Feed, FeedItem


@admin.register(Feed)
class FeedAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


@admin.register(FeedItem)
class FeedAdmin(admin.ModelAdmin):
    list_display = ('id', 'source_id', 'title', 'published')
