import pickle
from django.test import TestCase
from unittest.mock import MagicMock, patch
from model_mommy import mommy

from rss_reader_app.models import Feed, FeedItem
from rss_reader_app.tasks import feed_syncer


def mock_read_feed():
    with open('rss_reader_app/mock_data/read_feed_sample.pickle', 'rb') as file:
        read_feed = pickle.load(file)
        return MagicMock(**{'return_value': read_feed})


def mock_read_feed_with_seven_extra_item():
    with open('rss_reader_app/mock_data/read_feed_sample_with_seven_extra_item.pickle', 'rb') as file:
        read_feed = pickle.load(file)
        return MagicMock(**{'return_value': read_feed})


class FeedSyncerTestCase(TestCase):

    def setUp(self):
        self.feed = mommy.make(Feed)

    def test_feed_syncer(self):
        with patch('rss_reader_app.tasks.read_feed', mock_read_feed()):
            feed_syncer(self.feed.id)

            self.assertEqual(25, FeedItem.objects.filter(feed=self.feed).count())

        # call feed_syncer again with new mock
        with patch('rss_reader_app.tasks.read_feed', mock_read_feed_with_seven_extra_item()):
            count_of_feed_item_before_call = FeedItem.objects.filter(feed=self.feed).count()

            feed_syncer(self.feed.id)

            self.assertEqual(count_of_feed_item_before_call + 7, FeedItem.objects.filter(feed=self.feed).count())
