import datetime
from celery.utils.log import get_task_logger
from celery import shared_task

from rss_reader_app.models import Feed, FeedItem
from simple_rss_reader.celery import app
from utils.rss_reader import read_feed

logger = get_task_logger(__name__)


@shared_task
def feed_syncer(feed_id):
    """
    Sync feed of given id
    """

    now = datetime.datetime.now()
    try:
        feed = Feed.objects.get(pk=feed_id)
    except Feed.DoesNotExist:
        logger.info(f'Feed with id == {feed_id} does not exist.'.format(feed_id=feed_id))
        return

    logger.info('Starting synchronization of {feed}'.format(feed=feed))

    new_feed = read_feed(feed.path, feed.hash)

    if new_feed is None:
        logger.info('There is no data for the feed {feed} or it has not changed.'.format(feed=feed))

        feed.last_read = now
        feed.save()
        return

    new_feed_items = []
    if hasattr(new_feed, 'entries'):
        new_feed_items = new_feed.entries  # TODO x for x in entries if x.published > feed.last_update

    for new_feed_item in new_feed_items:
        feed_item = FeedItem.objects.create_if_required(feed, **new_feed_item)
        if feed_item:
            logger.info('Feed item {feed_item} of the feed {feed} successfully added.'.format(feed_item=feed_item, feed=feed))

    feed.last_read = now
    feed.hash = new_feed.get('hash', feed.hash)
    feed.save()
    logger.info('synchronization of {feed} finished.'.format(feed=feed))


@app.task(name='all_feeds_syncer')
def all_feeds_syncer():
    """
    Sync feeds from the Feed model with active == true
    """

    active_feeds = Feed.objects.filter(active=True).values_list('id', flat=True)

    logger.info('Start creating {active_feed_count} feed tasks ...'.format(active_feed_count=len(active_feeds)))

    for active_feed in active_feeds:
        feed_syncer.delay(feed_id=active_feed)
