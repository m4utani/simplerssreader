import datetime

from django.db import models

from utils.base_model import BaseModel


class Feed(BaseModel):  # based on RSS spec https://www.rssboard.org/rss-specification
    path = models.URLField(max_length=2048, unique=True, blank=False, null=False)

    description = models.CharField(max_length=500, blank=False, null=False)
    link = models.CharField(max_length=2048, blank=False, null=False)
    title = models.CharField(max_length=100, blank=False, null=False)
    version = models.CharField(max_length=20, blank=False, null=False)

    # last_read = models.DateTimeField(null=True, blank=True)
    hash = models.CharField(max_length=32, null=True,
                            blank=True)  # MD5 hash - Use to check if the feed has been modified. max_length based on https://stackoverflow.com/questions/3394503/maximum-length-for-md5-input-output
    active = models.BooleanField(null=False, blank=False, default=True)

    # Other fields ...

    def __str__(self):
        return self.path

    class Meta:
        ordering = ('-created',)


class FeedItemManager(models.Manager):
    def create_if_required(self, feed, **feed_item):

        source_id = feed_item.get('id', None)
        title = feed_item.get('title', None)
        link = feed_item.get('link', None)
        description = feed_item.get('description', None)
        author = feed_item.get('author', None)
        published = feed_item.get('published', None)

        if source_id:  # check existence by id
            feed_item_exist = self.model.objects.filter(source_id=source_id).exists()
            if feed_item_exist:
                return

        # check existence by other info
        feed_item_exist = self.model.objects.filter(feed=feed,
                                                    title=title,
                                                    description=description,
                                                    link=link).exists()
        if feed_item_exist:
            return

        feed_item = self.model.objects.create(feed=feed,
                                              source_id=source_id,
                                              title=title,
                                              link=link,
                                              description=description,
                                              author=author,
                                              published=published)
        return feed_item


class FeedItem(BaseModel):  # based on RSS epec https://www.rssboard.org/rss-specification#hrelementsOfLtitemgt
    feed = models.ForeignKey(Feed, models.CASCADE, related_name='items')

    source_id = models.CharField(max_length=500, blank=True, null=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    link = models.URLField(max_length=500, blank=True, null=True)
    description = models.CharField(max_length=500, blank=True, null=True)

    author = models.CharField(max_length=500, blank=True, null=True)
    published = models.CharField(max_length=500, null=True, blank=True)

    # Other fields ...

    objects = FeedItemManager()

    class Meta:
        ordering = ('-published', '-created')

    def clean(self):
        # Don't allow feed item with title and description == null
        if not self.title and not self.description:
            from django.core.exceptions import ValidationError
            raise ValidationError('At least one of title or description must be present')

    def __str__(self):
        return self.title if self.title else self.description[:100]
