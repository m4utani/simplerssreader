from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'simple_rss_reader.settings')
app = Celery('rss_reader_web', include=['rss_reader_app.tasks'])

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {

    'run_all_feeds_syncer': {
        'task': 'all_feeds_syncer',
        'schedule': 60,
    }
}
