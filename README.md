# SimpleRSSReader 

## Usage

To start project:

In the root directory, create a file called ‘.env’ and replace it with the following variables:

```
SECRET_KEY=secret
DEBUG=True
CELERYBEAT_SCHEDULER=django_celery_beat.schedulers:DatabaseScheduler
CELERY_BROKER_URL=redis://redis:6379
CELERY_RESULT_BACKEND=redis://redis:6379
CELERY_ACCEPT_CONTENT=application/json
CELERY_TASK_SERIALIZER=json
CELERY_RESULT_SERIALIZER=json
CELERY_TIMEZONE=UTC
```



Run: 
```sh
docker-compose up -d --build
```

Admin page => http://127.0.0.1/admin

* username: dev
* password: 123
